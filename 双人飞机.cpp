#define  _CRT_SECURE_NO_WARNINGS
#include<graphics.h>
#include<conio.h>
# include<math.h>
# include< stdio.h>
#include<windows.h>

// 引用 Windows Multimedia API
# pragma comment(lib,"Winmm.lib")

#define High 600           //游戏画面尺寸
#define Width 1000

IMAGE img_bk;                                    //背景图片
float position1_x, position1_y, position2_x, position2_y;                    //飞机的位置
float bullet1_x, bullet1_y, bullet2_x, bullet2_y;                        //子弹的位置
float enemy1_x, enemy1_y, enemy2_x, enemy2_y;                          //敌机的位置
float enemy_vy;
IMAGE img_planeNormal1, img_planeNormal2, img_planeNormal3, img_planeNormal4;        //正常飞机图片
IMAGE img_planeExplode1, img_planeExplode2;      //爆炸飞机图片
IMAGE img_bullet1, img_bullet2, img_bullet3, img_bullet4;                  //子弹图片
IMAGE img_enemyPlane1, img_enemyPlane2, img_enemyPlane3, img_enemyPlane4;          //敌机图片

int isExpolde = 0;                               // 飞机是否爆炸
int score = 0;                                     //得分
int flag;
               

int people = 0;  //人数，0为初始状态，1为单人游戏，2为双人游戏

int gameStatus = 0;//游戏状态,0为初始菜单界面,1为正常游戏,2为结束游戏状态,3为游戏暂停

void startMenu();               //初始菜单界面
void pauseMenu();               //游戏暂停后的菜单界面,一般按 Esc 键后启动该界面
void startup();                 //数据的初始化
void choose();
void judgement(float bullet_x, float enemy_x, float bullet_y, float enemy_y);
void show();                    //显示画面
void updateWithoutInput();      //与用户输入无关的更新
void updateWithInput();         //与用户输人有关的更新
void gameover();                //游戏结束,进行后续处理
void readRecordFile();          //读取游戏数据文件存档
void writeRecordFile();         //存储游戏数据文件存档

void startMenu()//初始菜单界面
{
	putimage(0, 0, &img_bk);
	setbkmode(TRANSPARENT);
	settextcolor(BLACK);
	settextstyle(50, 0, _T("黑体"));
	outtextxy(Width * 0.3, High * 0.2, "1 新游戏");
	outtextxy(Width * 0.3, High * 0.3, "2 读取游戏存档");
	outtextxy(Width * 0.3, High * 0.4, "3 退出");

	settextcolor(BLUE);
	settextstyle(30, 0, _T("黑体"));
	outtextxy(Width * 0.25, High * 0.6, "鼠标移动控制飞机移动");
	outtextxy(Width * 0.25, High * 0.65, "鼠标左键发射子弹");
	outtextxy(Width * 0.25, High * 0.7, "Ese暂停游戏");
	outtextxy(Width * 0.25, High * 0.75, "撞击后按任意键重新开始");
	FlushBatchDraw();
	Sleep(2);

	char input;
	if (_kbhit())
	{
		input = _getch();
		if (input == '1')
		{
			gameStatus = 1;

		}
		else if (input == '2')
		{
			readRecordFile();
			gameStatus = 1;
		}
		else if (input = '3')
		{
			gameStatus = 2;
			exit(0);
		}
		
	}
	
}

void pauseMenu()//游戏暂停后的菜单界面,一般按 Esc 键后启动该界面
{
	putimage(0, 0, &img_bk);
	setbkmode(TRANSPARENT);
	settextcolor(BLACK);
	settextstyle(50, 0, _T("黑体"));
	outtextxy(Width * 0.3, High * 0.2, "1 继续游戏");
	outtextxy(Width * 0.3, High * 0.3, "2 保存档案");
	outtextxy(Width * 0.3, High * 0.4, "3 退出");

	settextcolor(BLUE);
	settextstyle(30, 0, _T("黑体"));
	outtextxy(Width * 0.25, High * 0.6, "鼠标移动控制飞机移动");
	outtextxy(Width * 0.25, High * 0.65, "鼠标左键发射子弹");
	outtextxy(Width * 0.25, High * 0.7, "Esc键暂停游戏");
	outtextxy(Width * 0.25, High * 0.75, "撞击后按任意键重新开始");
	FlushBatchDraw();
	Sleep(2);

	char input;
	if (_kbhit())
	{
		input = _getch();
		if (input == '1')
			gameStatus = 1;
		else if (input == '2')
		{
			writeRecordFile();
			gameStatus = 1;
		}
		else if (input == '3')
		{
			gameStatus = 2;
			exit(0);
		}
	}
}
void readRecordFile()// 读取游戏数据文件存档
{
	FILE* fp;
	fp = fopen(".\\gamerecord.dat", "r");
	fscanf(fp, "%f %f %f %f %f %f %d %d", &position1_x, &position1_y, &bullet1_y, &enemy1_x, &enemy1_y, &isExpolde, &score);
	fclose(fp);
}

void writeRecordFile()//存储游戏数据文件存档
{
	FILE* fp;
	fp = fopen(".\\gameRecord.dat", "w");
	fprintf(fp, " %f %f %f %f %f %f %d %d", position1_x, position1_y, bullet1_x, bullet1_y, enemy1_x, enemy1_y, isExpolde, score);
	fclose(fp);
}
void startup()
{
	mciSendString("open .\\game_music.mp3 alias bkmusic", NULL, 0, NULL);//打开背景音乐 
	mciSendString("play bkmusic repeat", NULL, 0, NULL);//循环播放 

	initgraph(Width, High);
	//获取窗口句柄 
	HWND hwnd = GetHWnd();
	//设置窗口标题文字 
	SetWindowText(hwnd, "飞机大战 v1.0");
	loadimage(&img_bk, ".\\background.jpg");
	loadimage(&img_planeNormal1, ".\\planeNormal_1.PNG");
	loadimage(&img_planeNormal2, ".\\planeNormal_2.jpg");
	loadimage(&img_planeNormal3, ".\\planeNormal_3.PNG");
	loadimage(&img_planeNormal4, ".\\planeNormal_4.jpg");
	loadimage(&img_bullet1, ".\\bullet1.PNG");
	loadimage(&img_bullet2, ".\\bullet2.jpg");
	loadimage(&img_bullet3, ".\\bullet3.PNG");
	loadimage(&img_bullet4, ".\\bullet4.jpg");
	loadimage(&img_enemyPlane1, ".\\enemyPlane1.PNG");
	loadimage(&img_enemyPlane2, ".\\enemyPlane2.jpg");
	loadimage(&img_enemyPlane3, ".\\enemyPlane3.PNG");
		loadimage(&img_enemyPlane4, ".\\enemyPlane4.jpg");
	loadimage(&img_planeExplode1, ".\\planeExplode_1.jpg");
	loadimage(&img_planeExplode2, ".\\planeExplode_2,jpg");
	position1_x = Width * 0.3;
	position1_y = High * 0.7;
	position2_x = Width * 0.8;
	position2_y = High * 0.7;
	bullet1_x = position1_x;
	bullet1_y = -85;
	bullet2_x = position2_x;
	bullet2_y = -85;
	enemy1_x = Width * 0.3;
	enemy1_y = 10;
	enemy2_x = Width * 0.7;
	enemy2_y = 10;
	enemy_vy = 0.5;
	BeginBatchDraw();
	while (gameStatus == 0)
	{
		startMenu();//初始菜单界面 
	}
	while (people == 0)
		choose();
}
void show()
{
	while (gameStatus == 3)

		pauseMenu();//游戏暂停后的菜单界面,一般按 Esc 键后启动该界面

	putimage(0, 0, &img_bk);//显示背景
	if (isExpolde == 0)
	{
		putimage(position1_x - 50, position1_y - 60, &img_planeNormal1, NOTSRCERASE);//显示常飞机
		putimage(position1_x - 50, position1_y - 60, &img_planeNormal2, SRCINVERT);
		if (people == 2)
		{
			putimage(position2_x - 50, position2_y - 60, &img_planeNormal3, NOTSRCERASE);//显示常飞机
			putimage(position2_x - 50, position2_y - 60, &img_planeNormal4, SRCINVERT);
		}
		putimage(bullet1_x - 7, bullet1_y, &img_bullet1, NOTSRCERASE); //显示子弹
		putimage(bullet1_x - 7, bullet1_y, &img_bullet2, SRCINVERT);
		if (people == 2)
		{
			putimage(bullet2_x - 7, bullet2_y, &img_bullet3, NOTSRCERASE); //显示子弹
			putimage(bullet2_x - 7, bullet2_y, &img_bullet4, SRCINVERT);
		}
		putimage(enemy1_x, enemy1_y, &img_enemyPlane1, NOTSRCERASE); //显示敌机
		putimage(enemy1_x, enemy1_y, &img_enemyPlane2, SRCINVERT);
		putimage(enemy2_x, enemy2_y, &img_enemyPlane3, NOTSRCERASE); //显示敌机
		putimage(enemy2_x, enemy2_y, &img_enemyPlane4, SRCINVERT);
	}
	else
	{
		putimage(position1_x - 50, position1_y - 60, &img_planeExplode1, NOTSRCERASE);
		//显示爆炸飞机
		putimage(position1_x - 50, position1_y - 60, &img_planeExplode2, SRCINVERT);
	}

	settextcolor(RED);
	settextstyle(20, 0, _T("黑体"));
	outtextxy(Width * 0.48, High * 0.95, "得分:");
	char s[5];
	sprintf(s, "%d", score);
	outtextxy(Width * 0.55, High * 0.95, s);
	FlushBatchDraw();
	Sleep(2);
}
void judgement(float bullet_x, float enemy_x,float bullet_y,float enemy_y)
{
	if (fabs(bullet_x-enemy_x) + fabs(bullet_y - enemy_y) < 80)  //子弹击中敌机
	{
		flag = 1;
		mciSendString("stop gemusic", NULL, 0, NULL);   // 先把前面一次的音乐停止
		mciSendString("close gemusic", NULL, 0, NULL); // 先把前面一次的音乐关闭
		mciSendString("open .\\gotEnemy.mp3 alias gemusic", NULL, 0, NULL); // 打开跳动音乐
		mciSendString("play gemusic", NULL, 0, NULL); // 仅播放一次
		score++;

		if (score%50 == 0&&score%100!=0)
		{
			enemy_vy = enemy_vy + 0.5;
			mciSendString("stop 5music", NULL, 0, NULL);   // 先把前面一次的音乐停止
			mciSendString("close 5music", NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString("open .\\5.mp3 alias 5music", NULL, 0, NULL); // 打开跳动音乐
			mciSendString("play 5music", NULL, 0, NULL); // 仅播放一次
		}
		if (score %100==0)
		{
			enemy_vy++;
			mciSendString("stop 10music", NULL, 0, NULL);   // 先把前面一次的音乐停止
			mciSendString("close 10music", NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString("open .\\10.mp3 alias 10music", NULL, 0, NULL); // 打开跳动音乐
			mciSendString("play 10music", NULL, 0, NULL); // 仅播放一次
		}
	}
}
void updateWithoutInput()
{
	if (isExpolde == 0)
	{
		if (bullet1_y > -25)
		{
			bullet1_y = bullet1_y - 2;
			
		}
		if (bullet2_y > -25)
		{
			bullet2_y = bullet2_y - 2;
		}
		if (enemy1_y < High - 25)
		{
			enemy1_y = enemy1_y + enemy_vy;
		}
		else
		{
			score--;
			enemy1_y = 10;
		}
		if (enemy2_y < High - 25)
		{
			enemy2_y = enemy2_y + enemy_vy;
		}
		else
		{
			score--;
			enemy2_y = 10;
		}
		judgement(bullet1_x, enemy1_x, bullet1_y, enemy1_y);
		if (flag == 1)
		{
			enemy1_x = rand() % Width;
			enemy1_y = -40;
			bullet1_y = -85;
			flag = 0;
		}
		judgement(bullet1_x, enemy2_x, bullet1_y, enemy2_y);
		if (flag == 1)
		{
			enemy2_x = rand() % Width;
			enemy2_y = -40;
			bullet1_y = -85;
			flag = 0;
		}
		judgement(bullet2_x, enemy1_x, bullet2_y, enemy1_y);
		if (flag == 1)
		{
			enemy1_x = rand() % Width;
			enemy1_y = -40;
			bullet2_y = -85;
			flag = 0;
		}
		judgement(bullet2_x, enemy2_x, bullet2_y, enemy1_y);
		if (flag == 1)
		{
			enemy2_x = rand() % Width;
			enemy2_y = -40;
			bullet2_y = -85;
			flag =0;
		}


		if ((fabs(position1_x - enemy1_x) + fabs(position1_y - enemy1_y) < 150)
	|| (fabs(position1_x - enemy2_x) + fabs(position1_y - enemy2_y) < 150)||((fabs(position2_x - enemy1_x) + fabs(position2_y - enemy1_y) < 150)&&(people==2))||( (fabs(position2_x - enemy2_x) + fabs(position2_y - enemy2_y) < 150) && (people == 2)) ) // 敌机击中我们
		{
			isExpolde = 1;
			mciSendString("stop exmusic", NULL, 0, NULL);   // 先把前面一次的音乐停止
			mciSendString("close exmusic", NULL, 0, NULL); // 先把前面一次的音乐关闭
			mciSendString("open .\\explode.mp3 alias exmusic", NULL, 0, NULL); // 打开跳动音乐
			mciSendString("play exmusic", NULL, 0, NULL); // 仅播放一次		
		}
	}
}
void updateWithInput()
{
	if (isExpolde == 0)
	{
		if (people == 1)
		{
			MOUSEMSG m;//定义鼠标消息

			while (MouseHit())//这个函数用于检测当前是否有鼠标消息
			{
				m = GetMouseMsg();
				if (m.uMsg == WM_MOUSEMOVE)
				{
					//飞机的位置等于鼠标所在的位置
					position1_x = m.x;
					position1_y = m.y;
				}
				else if (m.uMsg == WM_LBUTTONDOWN)
				{
					//按下鼠标左键发射子弹
					bullet1_x = position1_x;
					bullet1_y = position1_y - 85;

					mciSendString("stop fgmusic", NULL, 0, NULL);//先把前面一次的音乐停止
					mciSendString("close fgmusic", NULL, 0, NULL);//先把前面一次的音乐关闭
					mciSendString("open .\\f_gun.mp3 alias fgmusic", NULL, 0, NULL);//打开跳动音乐

					mciSendString("play fgmusic", NULL, 0, NULL);	//仅播放一次
				}

			}
		}
		else
		{
			if ((GetAsyncKeyState(0x41) & 0x8000))//a
			{
				position1_x= position1_x - 2;
			}

			if ((GetAsyncKeyState(0x44) & 0x8000))// d
			{
				position1_x = position1_x+ 2;
			}

			if ((GetAsyncKeyState(0x57) & 0x8000))//w
			{
				position1_y = position1_y- 2;
			}

			if ((GetAsyncKeyState(0x53) & 0x8000))// s
			{
				position1_y = position1_y + 2;
			}
			if ((GetAsyncKeyState(0x20) & 0x8000))//

			{
				bullet1_x = position1_x;
				bullet1_y = position1_y - 85;
			}

				if ((GetAsyncKeyState(VK_LEFT) & 0x8000))	//左方向键
				{
					position2_x = position2_x - 2;
				}

				if ((GetAsyncKeyState(VK_RIGHT) & 0x8000))
				{
					position2_x = position2_x + 2;
				}

				if ((GetAsyncKeyState(VK_UP) & 0x8000))//上方向键
				{
					position2_y= position2_y - 2;
				}
				if ((GetAsyncKeyState(VK_DOWN) & 0x8000))//下方向键
				{
					position2_y= position2_y + 2;
				}
				if ((GetAsyncKeyState(0x30) & 0x8000))//a

				{
					bullet2_x = position2_x;
					bullet2_y = position2_y - 85;
				}
			
		}

		char input;// 判断是否有输入

		if (_kbhit())
		{
			input = _getch();//根据用户的不同输入来移动,不必输入回车

			if (input == 27)//Esc键的ACSII 码为27
			{
				gameStatus = 3;
			}
		}
	}
}
void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}
void choose()
{
	putimage(0, 0, &img_bk);
	setbkmode(TRANSPARENT);
	settextcolor(BLACK);
	settextstyle(50, 0, _T("黑体"));
	outtextxy(Width * 0.3, High * 0.2, "1 单人游戏");
	outtextxy(Width * 0.3, High * 0.3, "2 双人游戏");
	FlushBatchDraw();
	Sleep(2);

	char input;
	if (_kbhit())
	{
		input = _getch();
		if (input == '1')
		{
			people = 1;

		}
		else if (input == '2')
		{
			people = 2;
		}
	
	}
	
}
int main()
{
	startup();//数据的初始化 
	
	while (1)//游戏循环执行 
	{
		show();//显示画面 
		updateWithoutInput();//与用户输入无关的更新 
		updateWithInput();//与用户输入有关的更新 
	}
	gameover();//游戏结束，进行后续处理 
	return 0;
}
